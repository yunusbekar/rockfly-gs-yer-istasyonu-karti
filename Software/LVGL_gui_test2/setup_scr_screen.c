/*
 * Copyright 2022 NXP
 * SPDX-License-Identifier: MIT
 */

#include <lvgl.h>
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "custom.h"


void setup_scr_screen(lv_ui *ui){

	//Write codes screen
	ui->screen = lv_obj_create(NULL);

	//Write style state: LV_STATE_DEFAULT for style_screen_main_main_default
	static lv_style_t style_screen_main_main_default;
	lv_style_reset(&style_screen_main_main_default);
	lv_style_set_bg_color(&style_screen_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_bg_opa(&style_screen_main_main_default, 0);
	lv_obj_add_style(ui->screen, &style_screen_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_cont_top_bar
	ui->screen_cont_top_bar = lv_obj_create(ui->screen);
	lv_obj_set_pos(ui->screen_cont_top_bar, 0, 0);
	lv_obj_set_size(ui->screen_cont_top_bar, 320, 20);

	//Write style state: LV_STATE_DEFAULT for style_screen_cont_top_bar_main_main_default
	static lv_style_t style_screen_cont_top_bar_main_main_default;
	lv_style_reset(&style_screen_cont_top_bar_main_main_default);
	lv_style_set_radius(&style_screen_cont_top_bar_main_main_default, 0);
	lv_style_set_bg_color(&style_screen_cont_top_bar_main_main_default, lv_color_make(0x98, 0xc9, 0xeb));
	lv_style_set_bg_grad_color(&style_screen_cont_top_bar_main_main_default, lv_color_make(0x98, 0xc9, 0xeb));
	lv_style_set_bg_grad_dir(&style_screen_cont_top_bar_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_screen_cont_top_bar_main_main_default, 255);
	lv_style_set_border_color(&style_screen_cont_top_bar_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_border_width(&style_screen_cont_top_bar_main_main_default, 0);
	lv_style_set_border_opa(&style_screen_cont_top_bar_main_main_default, 255);
	lv_style_set_pad_left(&style_screen_cont_top_bar_main_main_default, 0);
	lv_style_set_pad_right(&style_screen_cont_top_bar_main_main_default, 0);
	lv_style_set_pad_top(&style_screen_cont_top_bar_main_main_default, 0);
	lv_style_set_pad_bottom(&style_screen_cont_top_bar_main_main_default, 0);
	lv_obj_add_style(ui->screen_cont_top_bar, &style_screen_cont_top_bar_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_cont_page
	ui->screen_cont_page = lv_obj_create(ui->screen);
	lv_obj_set_pos(ui->screen_cont_page, 0, 20);
	lv_obj_set_size(ui->screen_cont_page, 320, 220);

	//Write style state: LV_STATE_DEFAULT for style_screen_cont_page_main_main_default
	static lv_style_t style_screen_cont_page_main_main_default;
	lv_style_reset(&style_screen_cont_page_main_main_default);
	lv_style_set_radius(&style_screen_cont_page_main_main_default, 0);
	lv_style_set_bg_color(&style_screen_cont_page_main_main_default, lv_color_make(0xf6, 0xf6, 0xf6));
	lv_style_set_bg_grad_color(&style_screen_cont_page_main_main_default, lv_color_make(0xf6, 0xf6, 0xf6));
	lv_style_set_bg_grad_dir(&style_screen_cont_page_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_screen_cont_page_main_main_default, 255);
	lv_style_set_border_color(&style_screen_cont_page_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_border_width(&style_screen_cont_page_main_main_default, 0);
	lv_style_set_border_opa(&style_screen_cont_page_main_main_default, 255);
	lv_style_set_pad_left(&style_screen_cont_page_main_main_default, 0);
	lv_style_set_pad_right(&style_screen_cont_page_main_main_default, 0);
	lv_style_set_pad_top(&style_screen_cont_page_main_main_default, 0);
	lv_style_set_pad_bottom(&style_screen_cont_page_main_main_default, 0);
	lv_obj_add_style(ui->screen_cont_page, &style_screen_cont_page_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_gs_bat_img
	ui->screen_gs_bat_img = lv_img_create(ui->screen);
	lv_obj_set_pos(ui->screen_gs_bat_img, 5, 2);
	lv_obj_set_size(ui->screen_gs_bat_img, 16, 16);

	//Write style state: LV_STATE_DEFAULT for style_screen_gs_bat_img_main_main_default
	static lv_style_t style_screen_gs_bat_img_main_main_default;
	lv_style_reset(&style_screen_gs_bat_img_main_main_default);
	lv_style_set_img_recolor(&style_screen_gs_bat_img_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_screen_gs_bat_img_main_main_default, 0);
	lv_style_set_img_opa(&style_screen_gs_bat_img_main_main_default, 255);
	lv_obj_add_style(ui->screen_gs_bat_img, &style_screen_gs_bat_img_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_flag(ui->screen_gs_bat_img, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->screen_gs_bat_img,&status);
	lv_img_set_pivot(ui->screen_gs_bat_img, 0,0);
	lv_img_set_angle(ui->screen_gs_bat_img, 0);

	//Write codes screen_img_1
	ui->screen_img_1 = lv_img_create(ui->screen);
	lv_obj_set_pos(ui->screen_img_1, 25, 2);
	lv_obj_set_size(ui->screen_img_1, 16, 16);

	//Write style state: LV_STATE_DEFAULT for style_screen_img_1_main_main_default
	static lv_style_t style_screen_img_1_main_main_default;
	lv_style_reset(&style_screen_img_1_main_main_default);
	lv_style_set_img_recolor(&style_screen_img_1_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_screen_img_1_main_main_default, 0);
	lv_style_set_img_opa(&style_screen_img_1_main_main_default, 255);
	lv_obj_add_style(ui->screen_img_1, &style_screen_img_1_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_flag(ui->screen_img_1, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->screen_img_1,&low_battery_level1);
	lv_img_set_pivot(ui->screen_img_1, 0,0);
	lv_img_set_angle(ui->screen_img_1, 0);

	//Write codes screen_img_2
	ui->screen_img_2 = lv_img_create(ui->screen);
	lv_obj_set_pos(ui->screen_img_2, 80, 2);
	lv_obj_set_size(ui->screen_img_2, 16, 16);

	//Write style state: LV_STATE_DEFAULT for style_screen_img_2_main_main_default
	static lv_style_t style_screen_img_2_main_main_default;
	lv_style_reset(&style_screen_img_2_main_main_default);
	lv_style_set_img_recolor(&style_screen_img_2_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_screen_img_2_main_main_default, 0);
	lv_style_set_img_opa(&style_screen_img_2_main_main_default, 255);
	lv_obj_add_style(ui->screen_img_2, &style_screen_img_2_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_flag(ui->screen_img_2, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->screen_img_2,&satellite_gps2);
	lv_img_set_pivot(ui->screen_img_2, 0,0);
	lv_img_set_angle(ui->screen_img_2, 0);

	//Write codes screen_label_gps
	ui->screen_label_gps = lv_label_create(ui->screen);
	lv_obj_set_pos(ui->screen_label_gps, 100, 5);
	lv_obj_set_size(ui->screen_label_gps, 5, 10);
	lv_label_set_text(ui->screen_label_gps, "5");
	lv_label_set_long_mode(ui->screen_label_gps, LV_LABEL_LONG_WRAP);
	lv_obj_set_style_text_align(ui->screen_label_gps, LV_TEXT_ALIGN_CENTER, 0);

	//Write style state: LV_STATE_DEFAULT for style_screen_label_gps_main_main_default
	static lv_style_t style_screen_label_gps_main_main_default;
	lv_style_reset(&style_screen_label_gps_main_main_default);
	lv_style_set_radius(&style_screen_label_gps_main_main_default, 0);
	lv_style_set_bg_color(&style_screen_label_gps_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_color(&style_screen_label_gps_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_dir(&style_screen_label_gps_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_screen_label_gps_main_main_default, 0);
	lv_style_set_text_color(&style_screen_label_gps_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_text_font(&style_screen_label_gps_main_main_default, &lv_font_simsun_9);
	lv_style_set_text_letter_space(&style_screen_label_gps_main_main_default, 2);
	lv_style_set_pad_left(&style_screen_label_gps_main_main_default, 0);
	lv_style_set_pad_right(&style_screen_label_gps_main_main_default, 0);
	lv_style_set_pad_top(&style_screen_label_gps_main_main_default, 0);
	lv_style_set_pad_bottom(&style_screen_label_gps_main_main_default, 0);
	lv_obj_add_style(ui->screen_label_gps, &style_screen_label_gps_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_label_1
	ui->screen_label_1 = lv_label_create(ui->screen);
	lv_obj_set_pos(ui->screen_label_1, 225, 5);
	lv_obj_set_size(ui->screen_label_1, 90, 10);
	lv_label_set_text(ui->screen_label_1, "14:30 03.03.2022");
	lv_label_set_long_mode(ui->screen_label_1, LV_LABEL_LONG_WRAP);
	lv_obj_set_style_text_align(ui->screen_label_1, LV_TEXT_ALIGN_CENTER, 0);

	//Write style state: LV_STATE_DEFAULT for style_screen_label_1_main_main_default
	static lv_style_t style_screen_label_1_main_main_default;
	lv_style_reset(&style_screen_label_1_main_main_default);
	lv_style_set_radius(&style_screen_label_1_main_main_default, 0);
	lv_style_set_bg_color(&style_screen_label_1_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_color(&style_screen_label_1_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_dir(&style_screen_label_1_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_screen_label_1_main_main_default, 0);
	lv_style_set_text_color(&style_screen_label_1_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_text_font(&style_screen_label_1_main_main_default, &lv_font_simsun_9);
	lv_style_set_text_letter_space(&style_screen_label_1_main_main_default, 1);
	lv_style_set_pad_left(&style_screen_label_1_main_main_default, 0);
	lv_style_set_pad_right(&style_screen_label_1_main_main_default, 0);
	lv_style_set_pad_top(&style_screen_label_1_main_main_default, 0);
	lv_style_set_pad_bottom(&style_screen_label_1_main_main_default, 0);
	lv_obj_add_style(ui->screen_label_1, &style_screen_label_1_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_img_3
	ui->screen_img_3 = lv_img_create(ui->screen);
	lv_obj_set_pos(ui->screen_img_3, 120, 2);
	lv_obj_set_size(ui->screen_img_3, 16, 16);

	//Write style state: LV_STATE_DEFAULT for style_screen_img_3_main_main_default
	static lv_style_t style_screen_img_3_main_main_default;
	lv_style_reset(&style_screen_img_3_main_main_default);
	lv_style_set_img_recolor(&style_screen_img_3_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_screen_img_3_main_main_default, 0);
	lv_style_set_img_opa(&style_screen_img_3_main_main_default, 255);
	lv_obj_add_style(ui->screen_img_3, &style_screen_img_3_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_flag(ui->screen_img_3, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->screen_img_3,&wifi);
	lv_img_set_pivot(ui->screen_img_3, 0,0);
	lv_img_set_angle(ui->screen_img_3, 0);

	//Write codes screen_img_4
	ui->screen_img_4 = lv_img_create(ui->screen);
	lv_obj_set_pos(ui->screen_img_4, 150, 2);
	lv_obj_set_size(ui->screen_img_4, 16, 16);

	//Write style state: LV_STATE_DEFAULT for style_screen_img_4_main_main_default
	static lv_style_t style_screen_img_4_main_main_default;
	lv_style_reset(&style_screen_img_4_main_main_default);
	lv_style_set_img_recolor(&style_screen_img_4_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_screen_img_4_main_main_default, 0);
	lv_style_set_img_opa(&style_screen_img_4_main_main_default, 255);
	lv_obj_add_style(ui->screen_img_4, &style_screen_img_4_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_flag(ui->screen_img_4, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->screen_img_4,&micro_sd_card);
	lv_img_set_pivot(ui->screen_img_4, 0,0);
	lv_img_set_angle(ui->screen_img_4, 0);

	//Write codes screen_label_2
	ui->screen_label_2 = lv_label_create(ui->screen);
	lv_obj_set_pos(ui->screen_label_2, 166, 5);
	lv_obj_set_size(ui->screen_label_2, 20, 10);
	lv_label_set_text(ui->screen_label_2, "%60");
	lv_label_set_long_mode(ui->screen_label_2, LV_LABEL_LONG_WRAP);
	lv_obj_set_style_text_align(ui->screen_label_2, LV_TEXT_ALIGN_CENTER, 0);

	//Write style state: LV_STATE_DEFAULT for style_screen_label_2_main_main_default
	static lv_style_t style_screen_label_2_main_main_default;
	lv_style_reset(&style_screen_label_2_main_main_default);
	lv_style_set_radius(&style_screen_label_2_main_main_default, 0);
	lv_style_set_bg_color(&style_screen_label_2_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_color(&style_screen_label_2_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_dir(&style_screen_label_2_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_screen_label_2_main_main_default, 0);
	lv_style_set_text_color(&style_screen_label_2_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_text_font(&style_screen_label_2_main_main_default, &lv_font_simsun_9);
	lv_style_set_text_letter_space(&style_screen_label_2_main_main_default, 1);
	lv_style_set_pad_left(&style_screen_label_2_main_main_default, 0);
	lv_style_set_pad_right(&style_screen_label_2_main_main_default, 0);
	lv_style_set_pad_top(&style_screen_label_2_main_main_default, 0);
	lv_style_set_pad_bottom(&style_screen_label_2_main_main_default, 0);
	lv_obj_add_style(ui->screen_label_2, &style_screen_label_2_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_imgbtn_1
	ui->screen_imgbtn_1 = lv_imgbtn_create(ui->screen);
	lv_obj_set_pos(ui->screen_imgbtn_1, 12, 50);
	lv_obj_set_size(ui->screen_imgbtn_1, 64, 64);

	//Write style state: LV_STATE_DEFAULT for style_screen_imgbtn_1_main_main_default
	static lv_style_t style_screen_imgbtn_1_main_main_default;
	lv_style_reset(&style_screen_imgbtn_1_main_main_default);
	lv_style_set_text_color(&style_screen_imgbtn_1_main_main_default, lv_color_make(0xfa, 0x00, 0x00));
	lv_style_set_img_recolor(&style_screen_imgbtn_1_main_main_default, lv_color_make(0x25, 0x6f, 0x9d));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_1_main_main_default, 0);
	lv_style_set_img_opa(&style_screen_imgbtn_1_main_main_default, 255);
	lv_obj_add_style(ui->screen_imgbtn_1, &style_screen_imgbtn_1_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_PRESSED for style_screen_imgbtn_1_main_main_pressed
	static lv_style_t style_screen_imgbtn_1_main_main_pressed;
	lv_style_reset(&style_screen_imgbtn_1_main_main_pressed);
	lv_style_set_text_color(&style_screen_imgbtn_1_main_main_pressed, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_1_main_main_pressed, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_1_main_main_pressed, 0);
	lv_obj_add_style(ui->screen_imgbtn_1, &style_screen_imgbtn_1_main_main_pressed, LV_PART_MAIN|LV_STATE_PRESSED);

	//Write style state: LV_STATE_CHECKED for style_screen_imgbtn_1_main_main_checked
	static lv_style_t style_screen_imgbtn_1_main_main_checked;
	lv_style_reset(&style_screen_imgbtn_1_main_main_checked);
	lv_style_set_text_color(&style_screen_imgbtn_1_main_main_checked, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_1_main_main_checked, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_1_main_main_checked, 0);
	lv_obj_add_style(ui->screen_imgbtn_1, &style_screen_imgbtn_1_main_main_checked, LV_PART_MAIN|LV_STATE_CHECKED);
	lv_imgbtn_set_src(ui->screen_imgbtn_1, LV_IMGBTN_STATE_RELEASED, NULL, &settings64, NULL);
	lv_obj_add_flag(ui->screen_imgbtn_1, LV_OBJ_FLAG_CHECKABLE);

	//Write codes screen_imgbtn_2
	ui->screen_imgbtn_2 = lv_imgbtn_create(ui->screen);
	lv_obj_set_pos(ui->screen_imgbtn_2, 88, 50);
	lv_obj_set_size(ui->screen_imgbtn_2, 64, 64);

	//Write style state: LV_STATE_DEFAULT for style_screen_imgbtn_2_main_main_default
	static lv_style_t style_screen_imgbtn_2_main_main_default;
	lv_style_reset(&style_screen_imgbtn_2_main_main_default);
	lv_style_set_text_color(&style_screen_imgbtn_2_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor(&style_screen_imgbtn_2_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_2_main_main_default, 0);
	lv_style_set_img_opa(&style_screen_imgbtn_2_main_main_default, 255);
	lv_obj_add_style(ui->screen_imgbtn_2, &style_screen_imgbtn_2_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_PRESSED for style_screen_imgbtn_2_main_main_pressed
	static lv_style_t style_screen_imgbtn_2_main_main_pressed;
	lv_style_reset(&style_screen_imgbtn_2_main_main_pressed);
	lv_style_set_text_color(&style_screen_imgbtn_2_main_main_pressed, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_2_main_main_pressed, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_2_main_main_pressed, 0);
	lv_obj_add_style(ui->screen_imgbtn_2, &style_screen_imgbtn_2_main_main_pressed, LV_PART_MAIN|LV_STATE_PRESSED);

	//Write style state: LV_STATE_CHECKED for style_screen_imgbtn_2_main_main_checked
	static lv_style_t style_screen_imgbtn_2_main_main_checked;
	lv_style_reset(&style_screen_imgbtn_2_main_main_checked);
	lv_style_set_text_color(&style_screen_imgbtn_2_main_main_checked, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_2_main_main_checked, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_2_main_main_checked, 0);
	lv_obj_add_style(ui->screen_imgbtn_2, &style_screen_imgbtn_2_main_main_checked, LV_PART_MAIN|LV_STATE_CHECKED);
	lv_imgbtn_set_src(ui->screen_imgbtn_2, LV_IMGBTN_STATE_RELEASED, NULL, &navigation, NULL);
	lv_obj_add_flag(ui->screen_imgbtn_2, LV_OBJ_FLAG_CHECKABLE);

	//Write codes screen_imgbtn_3
	ui->screen_imgbtn_3 = lv_imgbtn_create(ui->screen);
	lv_obj_set_pos(ui->screen_imgbtn_3, 164, 50);
	lv_obj_set_size(ui->screen_imgbtn_3, 64, 64);

	//Write style state: LV_STATE_DEFAULT for style_screen_imgbtn_3_main_main_default
	static lv_style_t style_screen_imgbtn_3_main_main_default;
	lv_style_reset(&style_screen_imgbtn_3_main_main_default);
	lv_style_set_text_color(&style_screen_imgbtn_3_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor(&style_screen_imgbtn_3_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_3_main_main_default, 0);
	lv_style_set_img_opa(&style_screen_imgbtn_3_main_main_default, 255);
	lv_obj_add_style(ui->screen_imgbtn_3, &style_screen_imgbtn_3_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_PRESSED for style_screen_imgbtn_3_main_main_pressed
	static lv_style_t style_screen_imgbtn_3_main_main_pressed;
	lv_style_reset(&style_screen_imgbtn_3_main_main_pressed);
	lv_style_set_text_color(&style_screen_imgbtn_3_main_main_pressed, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_3_main_main_pressed, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_3_main_main_pressed, 0);
	lv_obj_add_style(ui->screen_imgbtn_3, &style_screen_imgbtn_3_main_main_pressed, LV_PART_MAIN|LV_STATE_PRESSED);

	//Write style state: LV_STATE_CHECKED for style_screen_imgbtn_3_main_main_checked
	static lv_style_t style_screen_imgbtn_3_main_main_checked;
	lv_style_reset(&style_screen_imgbtn_3_main_main_checked);
	lv_style_set_text_color(&style_screen_imgbtn_3_main_main_checked, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_3_main_main_checked, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_3_main_main_checked, 0);
	lv_obj_add_style(ui->screen_imgbtn_3, &style_screen_imgbtn_3_main_main_checked, LV_PART_MAIN|LV_STATE_CHECKED);
	lv_imgbtn_set_src(ui->screen_imgbtn_3, LV_IMGBTN_STATE_RELEASED, NULL, &alarmm, NULL);
	lv_imgbtn_set_src(ui->screen_imgbtn_3, LV_IMGBTN_STATE_PRESSED, NULL, &alarmm, NULL);
	lv_obj_add_flag(ui->screen_imgbtn_3, LV_OBJ_FLAG_CHECKABLE);

	//Write codes screen_imgbtn_4
	ui->screen_imgbtn_4 = lv_imgbtn_create(ui->screen);
	lv_obj_set_pos(ui->screen_imgbtn_4, 240, 140);
	lv_obj_set_size(ui->screen_imgbtn_4, 64, 64);

	//Write style state: LV_STATE_DEFAULT for style_screen_imgbtn_4_main_main_default
	static lv_style_t style_screen_imgbtn_4_main_main_default;
	lv_style_reset(&style_screen_imgbtn_4_main_main_default);
	lv_style_set_text_color(&style_screen_imgbtn_4_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor(&style_screen_imgbtn_4_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_4_main_main_default, 0);
	lv_style_set_img_opa(&style_screen_imgbtn_4_main_main_default, 255);
	lv_obj_add_style(ui->screen_imgbtn_4, &style_screen_imgbtn_4_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_PRESSED for style_screen_imgbtn_4_main_main_pressed
	static lv_style_t style_screen_imgbtn_4_main_main_pressed;
	lv_style_reset(&style_screen_imgbtn_4_main_main_pressed);
	lv_style_set_text_color(&style_screen_imgbtn_4_main_main_pressed, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_4_main_main_pressed, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_4_main_main_pressed, 0);
	lv_obj_add_style(ui->screen_imgbtn_4, &style_screen_imgbtn_4_main_main_pressed, LV_PART_MAIN|LV_STATE_PRESSED);

	//Write style state: LV_STATE_CHECKED for style_screen_imgbtn_4_main_main_checked
	static lv_style_t style_screen_imgbtn_4_main_main_checked;
	lv_style_reset(&style_screen_imgbtn_4_main_main_checked);
	lv_style_set_text_color(&style_screen_imgbtn_4_main_main_checked, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_4_main_main_checked, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_4_main_main_checked, 0);
	lv_obj_add_style(ui->screen_imgbtn_4, &style_screen_imgbtn_4_main_main_checked, LV_PART_MAIN|LV_STATE_CHECKED);
	lv_imgbtn_set_src(ui->screen_imgbtn_4, LV_IMGBTN_STATE_RELEASED, NULL, &send, NULL);
	lv_obj_add_flag(ui->screen_imgbtn_4, LV_OBJ_FLAG_CHECKABLE);

	//Write codes screen_imgbtn_5
	ui->screen_imgbtn_5 = lv_imgbtn_create(ui->screen);
	lv_obj_set_pos(ui->screen_imgbtn_5, 88, 140);
	lv_obj_set_size(ui->screen_imgbtn_5, 64, 64);

	//Write style state: LV_STATE_DEFAULT for style_screen_imgbtn_5_main_main_default
	static lv_style_t style_screen_imgbtn_5_main_main_default;
	lv_style_reset(&style_screen_imgbtn_5_main_main_default);
	lv_style_set_text_color(&style_screen_imgbtn_5_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor(&style_screen_imgbtn_5_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_5_main_main_default, 0);
	lv_style_set_img_opa(&style_screen_imgbtn_5_main_main_default, 255);
	lv_obj_add_style(ui->screen_imgbtn_5, &style_screen_imgbtn_5_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_PRESSED for style_screen_imgbtn_5_main_main_pressed
	static lv_style_t style_screen_imgbtn_5_main_main_pressed;
	lv_style_reset(&style_screen_imgbtn_5_main_main_pressed);
	lv_style_set_text_color(&style_screen_imgbtn_5_main_main_pressed, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_5_main_main_pressed, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_5_main_main_pressed, 0);
	lv_obj_add_style(ui->screen_imgbtn_5, &style_screen_imgbtn_5_main_main_pressed, LV_PART_MAIN|LV_STATE_PRESSED);

	//Write style state: LV_STATE_CHECKED for style_screen_imgbtn_5_main_main_checked
	static lv_style_t style_screen_imgbtn_5_main_main_checked;
	lv_style_reset(&style_screen_imgbtn_5_main_main_checked);
	lv_style_set_text_color(&style_screen_imgbtn_5_main_main_checked, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_5_main_main_checked, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_5_main_main_checked, 0);
	lv_obj_add_style(ui->screen_imgbtn_5, &style_screen_imgbtn_5_main_main_checked, LV_PART_MAIN|LV_STATE_CHECKED);
	lv_imgbtn_set_src(ui->screen_imgbtn_5, LV_IMGBTN_STATE_RELEASED, NULL, &compass, NULL);
	lv_obj_add_flag(ui->screen_imgbtn_5, LV_OBJ_FLAG_CHECKABLE);

	//Write codes screen_imgbtn_6
	ui->screen_imgbtn_6 = lv_imgbtn_create(ui->screen);
	lv_obj_set_pos(ui->screen_imgbtn_6, 164, 140);
	lv_obj_set_size(ui->screen_imgbtn_6, 64, 64);

	//Write style state: LV_STATE_DEFAULT for style_screen_imgbtn_6_main_main_default
	static lv_style_t style_screen_imgbtn_6_main_main_default;
	lv_style_reset(&style_screen_imgbtn_6_main_main_default);
	lv_style_set_text_color(&style_screen_imgbtn_6_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor(&style_screen_imgbtn_6_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_6_main_main_default, 0);
	lv_style_set_img_opa(&style_screen_imgbtn_6_main_main_default, 255);
	lv_obj_add_style(ui->screen_imgbtn_6, &style_screen_imgbtn_6_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_PRESSED for style_screen_imgbtn_6_main_main_pressed
	static lv_style_t style_screen_imgbtn_6_main_main_pressed;
	lv_style_reset(&style_screen_imgbtn_6_main_main_pressed);
	lv_style_set_text_color(&style_screen_imgbtn_6_main_main_pressed, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_6_main_main_pressed, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_6_main_main_pressed, 0);
	lv_obj_add_style(ui->screen_imgbtn_6, &style_screen_imgbtn_6_main_main_pressed, LV_PART_MAIN|LV_STATE_PRESSED);

	//Write style state: LV_STATE_CHECKED for style_screen_imgbtn_6_main_main_checked
	static lv_style_t style_screen_imgbtn_6_main_main_checked;
	lv_style_reset(&style_screen_imgbtn_6_main_main_checked);
	lv_style_set_text_color(&style_screen_imgbtn_6_main_main_checked, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_6_main_main_checked, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_6_main_main_checked, 0);
	lv_obj_add_style(ui->screen_imgbtn_6, &style_screen_imgbtn_6_main_main_checked, LV_PART_MAIN|LV_STATE_CHECKED);
	lv_imgbtn_set_src(ui->screen_imgbtn_6, LV_IMGBTN_STATE_RELEASED, NULL, &search, NULL);
	lv_obj_add_flag(ui->screen_imgbtn_6, LV_OBJ_FLAG_CHECKABLE);

	//Write codes screen_imgbtn_7
	ui->screen_imgbtn_7 = lv_imgbtn_create(ui->screen);
	lv_obj_set_pos(ui->screen_imgbtn_7, 240, 50);
	lv_obj_set_size(ui->screen_imgbtn_7, 64, 64);

	//Write style state: LV_STATE_DEFAULT for style_screen_imgbtn_7_main_main_default
	static lv_style_t style_screen_imgbtn_7_main_main_default;
	lv_style_reset(&style_screen_imgbtn_7_main_main_default);
	lv_style_set_text_color(&style_screen_imgbtn_7_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor(&style_screen_imgbtn_7_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_7_main_main_default, 0);
	lv_style_set_img_opa(&style_screen_imgbtn_7_main_main_default, 255);
	lv_obj_add_style(ui->screen_imgbtn_7, &style_screen_imgbtn_7_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_PRESSED for style_screen_imgbtn_7_main_main_pressed
	static lv_style_t style_screen_imgbtn_7_main_main_pressed;
	lv_style_reset(&style_screen_imgbtn_7_main_main_pressed);
	lv_style_set_text_color(&style_screen_imgbtn_7_main_main_pressed, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_7_main_main_pressed, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_7_main_main_pressed, 0);
	lv_obj_add_style(ui->screen_imgbtn_7, &style_screen_imgbtn_7_main_main_pressed, LV_PART_MAIN|LV_STATE_PRESSED);

	//Write style state: LV_STATE_CHECKED for style_screen_imgbtn_7_main_main_checked
	static lv_style_t style_screen_imgbtn_7_main_main_checked;
	lv_style_reset(&style_screen_imgbtn_7_main_main_checked);
	lv_style_set_text_color(&style_screen_imgbtn_7_main_main_checked, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_7_main_main_checked, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_7_main_main_checked, 0);
	lv_obj_add_style(ui->screen_imgbtn_7, &style_screen_imgbtn_7_main_main_checked, LV_PART_MAIN|LV_STATE_CHECKED);
	lv_imgbtn_set_src(ui->screen_imgbtn_7, LV_IMGBTN_STATE_RELEASED, NULL, &configuration, NULL);
	lv_obj_add_flag(ui->screen_imgbtn_7, LV_OBJ_FLAG_CHECKABLE);

	//Write codes screen_imgbtn_8
	ui->screen_imgbtn_8 = lv_imgbtn_create(ui->screen);
	lv_obj_set_pos(ui->screen_imgbtn_8, 12, 140);
	lv_obj_set_size(ui->screen_imgbtn_8, 64, 64);

	//Write style state: LV_STATE_DEFAULT for style_screen_imgbtn_8_main_main_default
	static lv_style_t style_screen_imgbtn_8_main_main_default;
	lv_style_reset(&style_screen_imgbtn_8_main_main_default);
	lv_style_set_text_color(&style_screen_imgbtn_8_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor(&style_screen_imgbtn_8_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_8_main_main_default, 0);
	lv_style_set_img_opa(&style_screen_imgbtn_8_main_main_default, 255);
	lv_obj_add_style(ui->screen_imgbtn_8, &style_screen_imgbtn_8_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_PRESSED for style_screen_imgbtn_8_main_main_pressed
	static lv_style_t style_screen_imgbtn_8_main_main_pressed;
	lv_style_reset(&style_screen_imgbtn_8_main_main_pressed);
	lv_style_set_text_color(&style_screen_imgbtn_8_main_main_pressed, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_8_main_main_pressed, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_8_main_main_pressed, 0);
	lv_obj_add_style(ui->screen_imgbtn_8, &style_screen_imgbtn_8_main_main_pressed, LV_PART_MAIN|LV_STATE_PRESSED);

	//Write style state: LV_STATE_CHECKED for style_screen_imgbtn_8_main_main_checked
	static lv_style_t style_screen_imgbtn_8_main_main_checked;
	lv_style_reset(&style_screen_imgbtn_8_main_main_checked);
	lv_style_set_text_color(&style_screen_imgbtn_8_main_main_checked, lv_color_make(0xFF, 0x33, 0xFF));
	lv_style_set_img_recolor(&style_screen_imgbtn_8_main_main_checked, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_img_recolor_opa(&style_screen_imgbtn_8_main_main_checked, 0);
	lv_obj_add_style(ui->screen_imgbtn_8, &style_screen_imgbtn_8_main_main_checked, LV_PART_MAIN|LV_STATE_CHECKED);
	lv_imgbtn_set_src(ui->screen_imgbtn_8, LV_IMGBTN_STATE_RELEASED, NULL, &tasks, NULL);
	lv_obj_add_flag(ui->screen_imgbtn_8, LV_OBJ_FLAG_CHECKABLE);

	//Init events for screen
	events_init_screen(ui);
}
