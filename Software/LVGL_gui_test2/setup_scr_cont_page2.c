/*
 * Copyright 2022 NXP
 * SPDX-License-Identifier: MIT
 */

#include <lvgl.h>
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "custom.h"


void setup_scr_cont_page2(lv_ui *ui){

	//Write codes cont_page2
	ui->cont_page2 = lv_obj_create(NULL);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_main_main_default
	static lv_style_t style_cont_page2_main_main_default;
	lv_style_reset(&style_cont_page2_main_main_default);
	lv_style_set_bg_color(&style_cont_page2_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_bg_opa(&style_cont_page2_main_main_default, 0);
	lv_obj_add_style(ui->cont_page2, &style_cont_page2_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes cont_page2_cont_top_bar
	ui->cont_page2_cont_top_bar = lv_obj_create(ui->cont_page2);
	lv_obj_set_pos(ui->cont_page2_cont_top_bar, 0, 0);
	lv_obj_set_size(ui->cont_page2_cont_top_bar, 320, 20);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_cont_top_bar_main_main_default
	static lv_style_t style_cont_page2_cont_top_bar_main_main_default;
	lv_style_reset(&style_cont_page2_cont_top_bar_main_main_default);
	lv_style_set_radius(&style_cont_page2_cont_top_bar_main_main_default, 0);
	lv_style_set_bg_color(&style_cont_page2_cont_top_bar_main_main_default, lv_color_make(0x98, 0xc9, 0xeb));
	lv_style_set_bg_grad_color(&style_cont_page2_cont_top_bar_main_main_default, lv_color_make(0x98, 0xc9, 0xeb));
	lv_style_set_bg_grad_dir(&style_cont_page2_cont_top_bar_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_cont_page2_cont_top_bar_main_main_default, 255);
	lv_style_set_border_color(&style_cont_page2_cont_top_bar_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_border_width(&style_cont_page2_cont_top_bar_main_main_default, 0);
	lv_style_set_border_opa(&style_cont_page2_cont_top_bar_main_main_default, 255);
	lv_style_set_pad_left(&style_cont_page2_cont_top_bar_main_main_default, 0);
	lv_style_set_pad_right(&style_cont_page2_cont_top_bar_main_main_default, 0);
	lv_style_set_pad_top(&style_cont_page2_cont_top_bar_main_main_default, 0);
	lv_style_set_pad_bottom(&style_cont_page2_cont_top_bar_main_main_default, 0);
	lv_obj_add_style(ui->cont_page2_cont_top_bar, &style_cont_page2_cont_top_bar_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes cont_page2_cont_page
	ui->cont_page2_cont_page = lv_obj_create(ui->cont_page2);
	lv_obj_set_pos(ui->cont_page2_cont_page, 0, 20);
	lv_obj_set_size(ui->cont_page2_cont_page, 320, 220);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_cont_page_main_main_default
	static lv_style_t style_cont_page2_cont_page_main_main_default;
	lv_style_reset(&style_cont_page2_cont_page_main_main_default);
	lv_style_set_radius(&style_cont_page2_cont_page_main_main_default, 0);
	lv_style_set_bg_color(&style_cont_page2_cont_page_main_main_default, lv_color_make(0xf6, 0xf6, 0xf6));
	lv_style_set_bg_grad_color(&style_cont_page2_cont_page_main_main_default, lv_color_make(0xf6, 0xf6, 0xf6));
	lv_style_set_bg_grad_dir(&style_cont_page2_cont_page_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_cont_page2_cont_page_main_main_default, 255);
	lv_style_set_border_color(&style_cont_page2_cont_page_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_border_width(&style_cont_page2_cont_page_main_main_default, 0);
	lv_style_set_border_opa(&style_cont_page2_cont_page_main_main_default, 255);
	lv_style_set_pad_left(&style_cont_page2_cont_page_main_main_default, 0);
	lv_style_set_pad_right(&style_cont_page2_cont_page_main_main_default, 0);
	lv_style_set_pad_top(&style_cont_page2_cont_page_main_main_default, 0);
	lv_style_set_pad_bottom(&style_cont_page2_cont_page_main_main_default, 0);
	lv_obj_add_style(ui->cont_page2_cont_page, &style_cont_page2_cont_page_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes cont_page2_gs_bat_img
	ui->cont_page2_gs_bat_img = lv_img_create(ui->cont_page2);
	lv_obj_set_pos(ui->cont_page2_gs_bat_img, 5, 2);
	lv_obj_set_size(ui->cont_page2_gs_bat_img, 16, 16);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_gs_bat_img_main_main_default
	static lv_style_t style_cont_page2_gs_bat_img_main_main_default;
	lv_style_reset(&style_cont_page2_gs_bat_img_main_main_default);
	lv_style_set_img_recolor(&style_cont_page2_gs_bat_img_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_cont_page2_gs_bat_img_main_main_default, 0);
	lv_style_set_img_opa(&style_cont_page2_gs_bat_img_main_main_default, 255);
	lv_obj_add_style(ui->cont_page2_gs_bat_img, &style_cont_page2_gs_bat_img_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_flag(ui->cont_page2_gs_bat_img, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->cont_page2_gs_bat_img,&status);
	lv_img_set_pivot(ui->cont_page2_gs_bat_img, 0,0);
	lv_img_set_angle(ui->cont_page2_gs_bat_img, 0);

	//Write codes cont_page2_img_1
	ui->cont_page2_img_1 = lv_img_create(ui->cont_page2);
	lv_obj_set_pos(ui->cont_page2_img_1, 25, 2);
	lv_obj_set_size(ui->cont_page2_img_1, 16, 16);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_img_1_main_main_default
	static lv_style_t style_cont_page2_img_1_main_main_default;
	lv_style_reset(&style_cont_page2_img_1_main_main_default);
	lv_style_set_img_recolor(&style_cont_page2_img_1_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_cont_page2_img_1_main_main_default, 0);
	lv_style_set_img_opa(&style_cont_page2_img_1_main_main_default, 255);
	lv_obj_add_style(ui->cont_page2_img_1, &style_cont_page2_img_1_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_flag(ui->cont_page2_img_1, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->cont_page2_img_1,&low_battery_level1);
	lv_img_set_pivot(ui->cont_page2_img_1, 0,0);
	lv_img_set_angle(ui->cont_page2_img_1, 0);

	//Write codes cont_page2_img_2
	ui->cont_page2_img_2 = lv_img_create(ui->cont_page2);
	lv_obj_set_pos(ui->cont_page2_img_2, 80, 2);
	lv_obj_set_size(ui->cont_page2_img_2, 16, 16);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_img_2_main_main_default
	static lv_style_t style_cont_page2_img_2_main_main_default;
	lv_style_reset(&style_cont_page2_img_2_main_main_default);
	lv_style_set_img_recolor(&style_cont_page2_img_2_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_cont_page2_img_2_main_main_default, 0);
	lv_style_set_img_opa(&style_cont_page2_img_2_main_main_default, 255);
	lv_obj_add_style(ui->cont_page2_img_2, &style_cont_page2_img_2_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_flag(ui->cont_page2_img_2, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->cont_page2_img_2,&satellite_gps2);
	lv_img_set_pivot(ui->cont_page2_img_2, 0,0);
	lv_img_set_angle(ui->cont_page2_img_2, 0);

	//Write codes cont_page2_label_gps
	ui->cont_page2_label_gps = lv_label_create(ui->cont_page2);
	lv_obj_set_pos(ui->cont_page2_label_gps, 100, 5);
	lv_obj_set_size(ui->cont_page2_label_gps, 5, 10);
	lv_label_set_text(ui->cont_page2_label_gps, "5");
	lv_label_set_long_mode(ui->cont_page2_label_gps, LV_LABEL_LONG_WRAP);
	lv_obj_set_style_text_align(ui->cont_page2_label_gps, LV_TEXT_ALIGN_CENTER, 0);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_label_gps_main_main_default
	static lv_style_t style_cont_page2_label_gps_main_main_default;
	lv_style_reset(&style_cont_page2_label_gps_main_main_default);
	lv_style_set_radius(&style_cont_page2_label_gps_main_main_default, 0);
	lv_style_set_bg_color(&style_cont_page2_label_gps_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_color(&style_cont_page2_label_gps_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_dir(&style_cont_page2_label_gps_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_cont_page2_label_gps_main_main_default, 0);
	lv_style_set_text_color(&style_cont_page2_label_gps_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_text_font(&style_cont_page2_label_gps_main_main_default, &lv_font_simsun_9);
	lv_style_set_text_letter_space(&style_cont_page2_label_gps_main_main_default, 2);
	lv_style_set_pad_left(&style_cont_page2_label_gps_main_main_default, 0);
	lv_style_set_pad_right(&style_cont_page2_label_gps_main_main_default, 0);
	lv_style_set_pad_top(&style_cont_page2_label_gps_main_main_default, 0);
	lv_style_set_pad_bottom(&style_cont_page2_label_gps_main_main_default, 0);
	lv_obj_add_style(ui->cont_page2_label_gps, &style_cont_page2_label_gps_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes cont_page2_label_1
	ui->cont_page2_label_1 = lv_label_create(ui->cont_page2);
	lv_obj_set_pos(ui->cont_page2_label_1, 225, 5);
	lv_obj_set_size(ui->cont_page2_label_1, 90, 10);
	lv_label_set_text(ui->cont_page2_label_1, "14:30 03.03.2022");
	lv_label_set_long_mode(ui->cont_page2_label_1, LV_LABEL_LONG_WRAP);
	lv_obj_set_style_text_align(ui->cont_page2_label_1, LV_TEXT_ALIGN_CENTER, 0);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_label_1_main_main_default
	static lv_style_t style_cont_page2_label_1_main_main_default;
	lv_style_reset(&style_cont_page2_label_1_main_main_default);
	lv_style_set_radius(&style_cont_page2_label_1_main_main_default, 0);
	lv_style_set_bg_color(&style_cont_page2_label_1_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_color(&style_cont_page2_label_1_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_dir(&style_cont_page2_label_1_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_cont_page2_label_1_main_main_default, 0);
	lv_style_set_text_color(&style_cont_page2_label_1_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_text_font(&style_cont_page2_label_1_main_main_default, &lv_font_simsun_9);
	lv_style_set_text_letter_space(&style_cont_page2_label_1_main_main_default, 1);
	lv_style_set_pad_left(&style_cont_page2_label_1_main_main_default, 0);
	lv_style_set_pad_right(&style_cont_page2_label_1_main_main_default, 0);
	lv_style_set_pad_top(&style_cont_page2_label_1_main_main_default, 0);
	lv_style_set_pad_bottom(&style_cont_page2_label_1_main_main_default, 0);
	lv_obj_add_style(ui->cont_page2_label_1, &style_cont_page2_label_1_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes cont_page2_img_3
	ui->cont_page2_img_3 = lv_img_create(ui->cont_page2);
	lv_obj_set_pos(ui->cont_page2_img_3, 120, 2);
	lv_obj_set_size(ui->cont_page2_img_3, 16, 16);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_img_3_main_main_default
	static lv_style_t style_cont_page2_img_3_main_main_default;
	lv_style_reset(&style_cont_page2_img_3_main_main_default);
	lv_style_set_img_recolor(&style_cont_page2_img_3_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_cont_page2_img_3_main_main_default, 0);
	lv_style_set_img_opa(&style_cont_page2_img_3_main_main_default, 255);
	lv_obj_add_style(ui->cont_page2_img_3, &style_cont_page2_img_3_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_flag(ui->cont_page2_img_3, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->cont_page2_img_3,&wifi);
	lv_img_set_pivot(ui->cont_page2_img_3, 0,0);
	lv_img_set_angle(ui->cont_page2_img_3, 0);

	//Write codes cont_page2_win_1
	ui->cont_page2_win_1 = lv_win_create(ui->cont_page2, 25);
	lv_win_add_title(ui->cont_page2_win_1, "Time Zone Settings");
	lv_obj_set_pos(ui->cont_page2_win_1, 0, 18);
	lv_obj_set_size(ui->cont_page2_win_1, 320, 220);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_win_1_main_main_default
	static lv_style_t style_cont_page2_win_1_main_main_default;
	lv_style_reset(&style_cont_page2_win_1_main_main_default);
	lv_style_set_bg_color(&style_cont_page2_win_1_main_main_default, lv_color_make(0xee, 0xee, 0xf6));
	lv_style_set_bg_grad_color(&style_cont_page2_win_1_main_main_default, lv_color_make(0xee, 0xee, 0xf6));
	lv_style_set_bg_grad_dir(&style_cont_page2_win_1_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_cont_page2_win_1_main_main_default, 255);
	lv_style_set_outline_color(&style_cont_page2_win_1_main_main_default, lv_color_make(0x08, 0x1A, 0x0F));
	lv_style_set_outline_width(&style_cont_page2_win_1_main_main_default, 0);
	lv_obj_add_style(ui->cont_page2_win_1, &style_cont_page2_win_1_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_win_1_extra_content_main_default
	static lv_style_t style_cont_page2_win_1_extra_content_main_default;
	lv_style_reset(&style_cont_page2_win_1_extra_content_main_default);
	lv_style_set_bg_color(&style_cont_page2_win_1_extra_content_main_default, lv_color_make(0xee, 0xee, 0xf6));
	lv_style_set_bg_grad_color(&style_cont_page2_win_1_extra_content_main_default, lv_color_make(0xee, 0xee, 0xf6));
	lv_style_set_bg_grad_dir(&style_cont_page2_win_1_extra_content_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_cont_page2_win_1_extra_content_main_default, 255);
	lv_style_set_text_color(&style_cont_page2_win_1_extra_content_main_default, lv_color_make(0x39, 0x3c, 0x41));
	lv_style_set_text_font(&style_cont_page2_win_1_extra_content_main_default, &lv_font_simsun_12);
	lv_style_set_text_letter_space(&style_cont_page2_win_1_extra_content_main_default, 0);
	lv_style_set_text_line_space(&style_cont_page2_win_1_extra_content_main_default, 2);
	lv_obj_add_style(lv_win_get_content(ui->cont_page2_win_1), &style_cont_page2_win_1_extra_content_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_win_1_extra_header_main_default
	static lv_style_t style_cont_page2_win_1_extra_header_main_default;
	lv_style_reset(&style_cont_page2_win_1_extra_header_main_default);
	lv_style_set_bg_color(&style_cont_page2_win_1_extra_header_main_default, lv_color_make(0xe6, 0xe6, 0xe6));
	lv_style_set_bg_grad_color(&style_cont_page2_win_1_extra_header_main_default, lv_color_make(0xe6, 0xe6, 0xe6));
	lv_style_set_bg_grad_dir(&style_cont_page2_win_1_extra_header_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_cont_page2_win_1_extra_header_main_default, 255);
	lv_style_set_text_color(&style_cont_page2_win_1_extra_header_main_default, lv_color_make(0x39, 0x3c, 0x41));
	lv_style_set_text_font(&style_cont_page2_win_1_extra_header_main_default, &lv_font_simsun_12);
	lv_style_set_text_letter_space(&style_cont_page2_win_1_extra_header_main_default, 0);
	lv_style_set_text_line_space(&style_cont_page2_win_1_extra_header_main_default, 2);
	lv_obj_add_style(lv_win_get_header(ui->cont_page2_win_1), &style_cont_page2_win_1_extra_header_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_win_1_extra_btns_main_default
	static lv_style_t style_cont_page2_win_1_extra_btns_main_default;
	lv_style_reset(&style_cont_page2_win_1_extra_btns_main_default);
	lv_style_set_radius(&style_cont_page2_win_1_extra_btns_main_default, 8);
	lv_style_set_bg_color(&style_cont_page2_win_1_extra_btns_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_color(&style_cont_page2_win_1_extra_btns_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_dir(&style_cont_page2_win_1_extra_btns_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_cont_page2_win_1_extra_btns_main_default, 255);
	lv_style_set_shadow_width(&style_cont_page2_win_1_extra_btns_main_default, 0);
	lv_obj_t *cont_page2_win_1_btn;
	cont_page2_win_1_btn = lv_win_add_btn(ui->cont_page2_win_1, LV_SYMBOL_CLOSE, 40);
	lv_obj_add_style(cont_page2_win_1_btn, &style_cont_page2_win_1_extra_btns_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_t *cont_page2_win_1_label = lv_label_create(lv_win_get_content(ui->cont_page2_win_1));
	lv_label_set_text(cont_page2_win_1_label, "");

	//Write codes cont_page2_img_4
	ui->cont_page2_img_4 = lv_img_create(ui->cont_page2);
	lv_obj_set_pos(ui->cont_page2_img_4, 150, 2);
	lv_obj_set_size(ui->cont_page2_img_4, 16, 16);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_img_4_main_main_default
	static lv_style_t style_cont_page2_img_4_main_main_default;
	lv_style_reset(&style_cont_page2_img_4_main_main_default);
	lv_style_set_img_recolor(&style_cont_page2_img_4_main_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_img_recolor_opa(&style_cont_page2_img_4_main_main_default, 0);
	lv_style_set_img_opa(&style_cont_page2_img_4_main_main_default, 255);
	lv_obj_add_style(ui->cont_page2_img_4, &style_cont_page2_img_4_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_flag(ui->cont_page2_img_4, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->cont_page2_img_4,&micro_sd_card);
	lv_img_set_pivot(ui->cont_page2_img_4, 0,0);
	lv_img_set_angle(ui->cont_page2_img_4, 0);

	//Write codes cont_page2_label_2
	ui->cont_page2_label_2 = lv_label_create(ui->cont_page2);
	lv_obj_set_pos(ui->cont_page2_label_2, 166, 5);
	lv_obj_set_size(ui->cont_page2_label_2, 20, 10);
	lv_label_set_text(ui->cont_page2_label_2, "%60");
	lv_label_set_long_mode(ui->cont_page2_label_2, LV_LABEL_LONG_WRAP);
	lv_obj_set_style_text_align(ui->cont_page2_label_2, LV_TEXT_ALIGN_CENTER, 0);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_label_2_main_main_default
	static lv_style_t style_cont_page2_label_2_main_main_default;
	lv_style_reset(&style_cont_page2_label_2_main_main_default);
	lv_style_set_radius(&style_cont_page2_label_2_main_main_default, 0);
	lv_style_set_bg_color(&style_cont_page2_label_2_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_color(&style_cont_page2_label_2_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_dir(&style_cont_page2_label_2_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_cont_page2_label_2_main_main_default, 0);
	lv_style_set_text_color(&style_cont_page2_label_2_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_text_font(&style_cont_page2_label_2_main_main_default, &lv_font_simsun_9);
	lv_style_set_text_letter_space(&style_cont_page2_label_2_main_main_default, 1);
	lv_style_set_pad_left(&style_cont_page2_label_2_main_main_default, 0);
	lv_style_set_pad_right(&style_cont_page2_label_2_main_main_default, 0);
	lv_style_set_pad_top(&style_cont_page2_label_2_main_main_default, 0);
	lv_style_set_pad_bottom(&style_cont_page2_label_2_main_main_default, 0);
	lv_obj_add_style(ui->cont_page2_label_2, &style_cont_page2_label_2_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes cont_page2_btn_1
	ui->cont_page2_btn_1 = lv_btn_create(ui->cont_page2);
	lv_obj_set_pos(ui->cont_page2_btn_1, 36, 100);
	lv_obj_set_size(ui->cont_page2_btn_1, 33, 33);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_btn_1_main_main_default
	static lv_style_t style_cont_page2_btn_1_main_main_default;
	lv_style_reset(&style_cont_page2_btn_1_main_main_default);
	lv_style_set_radius(&style_cont_page2_btn_1_main_main_default, 5);
	lv_style_set_bg_color(&style_cont_page2_btn_1_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_color(&style_cont_page2_btn_1_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_dir(&style_cont_page2_btn_1_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_cont_page2_btn_1_main_main_default, 255);
	lv_style_set_shadow_color(&style_cont_page2_btn_1_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_shadow_opa(&style_cont_page2_btn_1_main_main_default, 255);
	lv_style_set_border_color(&style_cont_page2_btn_1_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_border_width(&style_cont_page2_btn_1_main_main_default, 0);
	lv_style_set_border_opa(&style_cont_page2_btn_1_main_main_default, 255);
	lv_obj_add_style(ui->cont_page2_btn_1, &style_cont_page2_btn_1_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	ui->cont_page2_btn_1_label = lv_label_create(ui->cont_page2_btn_1);
	lv_label_set_text(ui->cont_page2_btn_1_label, "-");
	lv_obj_set_style_text_color(ui->cont_page2_btn_1_label, lv_color_make(0xfa, 0xf9, 0xf9), LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->cont_page2_btn_1_label, &lv_font_simsun_20, LV_STATE_DEFAULT);
	lv_obj_set_style_pad_all(ui->cont_page2_btn_1, 0, LV_STATE_DEFAULT);
	lv_obj_align(ui->cont_page2_btn_1_label, LV_ALIGN_CENTER, 0, 0);

	//Write codes cont_page2_btn_2
	ui->cont_page2_btn_2 = lv_btn_create(ui->cont_page2);
	lv_obj_set_pos(ui->cont_page2_btn_2, 230, 100);
	lv_obj_set_size(ui->cont_page2_btn_2, 33, 33);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_btn_2_main_main_default
	static lv_style_t style_cont_page2_btn_2_main_main_default;
	lv_style_reset(&style_cont_page2_btn_2_main_main_default);
	lv_style_set_radius(&style_cont_page2_btn_2_main_main_default, 5);
	lv_style_set_bg_color(&style_cont_page2_btn_2_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_color(&style_cont_page2_btn_2_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_dir(&style_cont_page2_btn_2_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_cont_page2_btn_2_main_main_default, 255);
	lv_style_set_shadow_color(&style_cont_page2_btn_2_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_shadow_opa(&style_cont_page2_btn_2_main_main_default, 255);
	lv_style_set_border_color(&style_cont_page2_btn_2_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_border_width(&style_cont_page2_btn_2_main_main_default, 0);
	lv_style_set_border_opa(&style_cont_page2_btn_2_main_main_default, 255);
	lv_obj_add_style(ui->cont_page2_btn_2, &style_cont_page2_btn_2_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
	ui->cont_page2_btn_2_label = lv_label_create(ui->cont_page2_btn_2);
	lv_label_set_text(ui->cont_page2_btn_2_label, "+");
	lv_obj_set_style_text_color(ui->cont_page2_btn_2_label, lv_color_make(0xff, 0xff, 0xff), LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->cont_page2_btn_2_label, &lv_font_simsun_20, LV_STATE_DEFAULT);
	lv_obj_set_style_pad_all(ui->cont_page2_btn_2, 0, LV_STATE_DEFAULT);
	lv_obj_align(ui->cont_page2_btn_2_label, LV_ALIGN_CENTER, 0, 0);

	//Write codes cont_page2_label_3
	ui->cont_page2_label_3 = lv_label_create(ui->cont_page2);
	lv_obj_set_pos(ui->cont_page2_label_3, 100, 101);
	lv_obj_set_size(ui->cont_page2_label_3, 100, 32);
	lv_label_set_text(ui->cont_page2_label_3, "0");
	lv_label_set_long_mode(ui->cont_page2_label_3, LV_LABEL_LONG_WRAP);
	lv_obj_set_style_text_align(ui->cont_page2_label_3, LV_TEXT_ALIGN_CENTER, 0);

	//Write style state: LV_STATE_DEFAULT for style_cont_page2_label_3_main_main_default
	static lv_style_t style_cont_page2_label_3_main_main_default;
	lv_style_reset(&style_cont_page2_label_3_main_main_default);
	lv_style_set_radius(&style_cont_page2_label_3_main_main_default, 0);
	lv_style_set_bg_color(&style_cont_page2_label_3_main_main_default, lv_color_make(0x6b, 0x7e, 0x85));
	lv_style_set_bg_grad_color(&style_cont_page2_label_3_main_main_default, lv_color_make(0x6b, 0x7e, 0x85));
	lv_style_set_bg_grad_dir(&style_cont_page2_label_3_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_cont_page2_label_3_main_main_default, 255);
	lv_style_set_text_color(&style_cont_page2_label_3_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_text_font(&style_cont_page2_label_3_main_main_default, &lv_font_simsun_12);
	lv_style_set_text_letter_space(&style_cont_page2_label_3_main_main_default, 2);
	lv_style_set_pad_left(&style_cont_page2_label_3_main_main_default, 0);
	lv_style_set_pad_right(&style_cont_page2_label_3_main_main_default, 0);
	lv_style_set_pad_top(&style_cont_page2_label_3_main_main_default, 0);
	lv_style_set_pad_bottom(&style_cont_page2_label_3_main_main_default, 0);
	lv_obj_add_style(ui->cont_page2_label_3, &style_cont_page2_label_3_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Init events for screen
	events_init_cont_page2(ui);
}
