/*
   RadioLib SX126x Transmit with Interrupts Example

   This example transmits LoRa packets with one second delays
   between them. Each packet contains up to 256 bytes
   of data, in the form of:
    - Arduino String
    - null-terminated char array (C-string)
    - arbitrary binary data (byte array)

   Other modules from SX126x family can also be used.

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

// include the library
#include <RadioLib.h>
#include "SPI.h"
SPIClass SPI2(HSPI);

// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  17
// NRST pin:  27
// BUSY pin:  39
SX1262 radio = new Module(15, 17, 27, 39,SPI2);

TaskHandle_t Task1;
#include <Wire.h>
#include <LSM303.h>

LSM303 compass;

// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1262 radio = RadioShield.ModuleA;

// save transmission state between loops
int transmissionState = ERR_NONE;


struct Data{
byte byteArray[76];
byte tr_array[];
int counter=0;
};
  Data data;

void setup() {
  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;  
  const int HSPI_CS = 15;

  SPI2.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS); 
  
  delay(1000);
  Serial.begin(115200);
pinMode(26,OUTPUT);

  
  // initialize SX1262 with default settings
  Serial.print(F("[SX1262] Initializing ... "));
  
  int state = radio.begin(868.0, 500.0, 7, 5, 0x34, 20);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }

  // set the function that will be called
  // when packet transmission is finished
  radio.setDio1Action(setFlag);

 xTaskCreatePinnedToCore(
                    Task1code,   /* Task function. */
                    "Task1",     /* name of task. */
                    10000,       /* Stack size of task */
                    NULL,        /* parameter of the task */
                    1,           /* priority of the task */
                    &Task1,      /* Task handle to keep track of created task */
                    0);          /* pin task to core 0 */                  
  delay(100); 
  
    floatAddByte(0.49,&data.counter,data.byteArray);
    floatAddByte(-0.49,&data.counter,data.byteArray);
    floatAddByte(1.22,&data.counter,data.byteArray);

    floatAddByte(36.52,&data.counter,data.byteArray);
    floatAddByte(996.33,&data.counter,data.byteArray);

    floatAddByte(-0.46,&data.counter,data.byteArray);
    floatAddByte(-0.14,&data.counter,data.byteArray);
    floatAddByte(-9.66,&data.counter,data.byteArray);
    floatAddByte(-0.00,&data.counter,data.byteArray);
    floatAddByte(0.00,&data.counter,data.byteArray);
    floatAddByte(0.00,&data.counter,data.byteArray);
    floatAddByte(-25.69,&data.counter,data.byteArray);
    floatAddByte(-35.19,&data.counter,data.byteArray);
    floatAddByte(12.44,&data.counter,data.byteArray);
    floatAddByte(-179.00,&data.counter,data.byteArray);
    floatAddByte(2.93,&data.counter,data.byteArray);
    floatAddByte(-33.18,&data.counter,data.byteArray);

    longAddByte(-118334965,&data.counter,data.byteArray);
    longAddByte(95731051,&data.counter,data.byteArray);
    //longAddByte(892087,&data.counter,data.byteArray);
    //byteAddByte(0,&data.counter,data.byteArray);
    //longAddByte(2021824,&data.counter,data.byteArray);
   // longAddByte(14564,&data.counter,data.byteArray);

    transmissionState = radio.startTransmit(data.byteArray,76);
}

// flag to indicate that a packet was sent
volatile bool transmittedFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// this function is called when a complete packet
// is transmitted by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if(!enableInterrupt) {
    return;
  }

  // we sent a packet, set the flag
  transmittedFlag = true;
}

void Task1code( void * pvParameters ){
 // Serial.print("Task1 running on core ");
 // Serial.println(xPortGetCoreID());
  
  for(;;){
    vTaskDelay(3);

  // check if the previous transmission finished
  if(transmittedFlag) {
    // disable the interrupt service routine while
    // processing the data
    enableInterrupt = false;

    // reset flag
    transmittedFlag = false;

    if (transmissionState == ERR_NONE) {
      // packet was successfully sent
      Serial.println(F("transmission finished!"));

      // NOTE: when using interrupt-driven transmit method,
      //       it is not possible to automatically measure
      //       transmission data rate using getDataRate()

    } else {
      Serial.print(F("failed, code "));
      Serial.println(transmissionState);

    }

    // wait a second before transmitting again
    delay(10);

    // send another one
    Serial.print(F("[SX1262] Sending another packet ... "));

    // you can transmit C-string or Arduino string up to
    // 256 characters long
    transmissionState = radio.startTransmit(data.byteArray,100);
 

    // we're ready to send more packets,
    // enable interrupt service routine
    enableInterrupt = true;
  }
     
   
 
  } 
}


void loop() {

  
  
}


void floatAddByte(float data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void intAddByte(int data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data,int *count,byte *array){
    array[*count]=data;
    (*count)++;
}
