

#include <Adafruit_GFX.h>
#include <MCUFRIEND_kbv.h>
MCUFRIEND_kbv tft;
#include <TouchScreen.h>
#include <Fonts/FreeSerifItalic12pt7b.h>


#include <Fonts/FreeSerifBold12pt7b.h>
#include <Fonts/FreeMonoBold12pt7b.h>
#include <Fonts/FreeSerif9pt7b.h>


#define MINPRESSURE 100
#define MAXPRESSURE 1000
int plus1;
int percent;
float a;
int degree=67;
// ALL Touch panels and wiring is DIFFERENT
// copy-paste results from TouchScreen_Calibr_native.ino
const int XP = 7, XM = A1, YP = A2, YM = 6; //ID=0x9341
const int TS_LEFT = 136, TS_RT = 907, TS_TOP = 942, TS_BOT = 139;

TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);

Adafruit_GFX_Button on_btn, zero_btn, off_btn;

int pixel_x, pixel_y;     //Touch_getXY() updates global vars
bool Touch_getXY(void)
{
    TSPoint p = ts.getPoint();
    pinMode(YP, OUTPUT);      //restore shared pins
    pinMode(XM, OUTPUT);
    digitalWrite(YP, HIGH);   //because TFT control pins
    digitalWrite(XM, HIGH);
    bool pressed = (p.z > MINPRESSURE && p.z < MAXPRESSURE);
    if (pressed) {
        pixel_x = map(p.x, TS_LEFT, TS_RT, 0, tft.width()); //.kbv makes sense to me
        pixel_y = map(p.y, TS_TOP, TS_BOT, 0, tft.height());
    }
    return pressed;
}
int sarz;
#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
#define GRAY     0xC618
#define DARKGRAY 0x6B0C
#define ORANGE   0xFD20 

//yer istasyonu tablo konum tanımlamaları
#define g_stnalt_x     246
#define g_stnalt_y     80

#define g_stnalt_en    230
#define g_stnalt_boy   193
#define g_barboy       43


//roket yer istasyonu yer tanımlamarı
#define r_stnalt_x     2
#define r_stnalt_y     80

#define r_stnalt_en    230
#define r_stnalt_boy   193
#define r_barboy       43


void setup(void)
{
    Serial.begin(9600);
    uint16_t ID = tft.readID();
    Serial.print("TFT ID = 0x");
    Serial.println(ID, HEX);
    Serial.println("Calibrate for your Touch Panel");
    if (ID == 0xD3D3) ID = 0x9486; // write-only shield
    tft.begin(ID);
    tft.setRotation(1);            //PORTRAIT
    tft.fillScreen(BLACK);
    
    on_btn.initButton(&tft,  55, 300, 100, 40, WHITE, CYAN, BLACK, "+", 2);
    off_btn.initButton(&tft, 165, 300, 100, 40, WHITE, CYAN, BLACK, "-", 2);
    zero_btn.initButton(&tft, 350, 300, 110, 40, WHITE, RED, BLACK, "SIFIRLA", 2);
    
    on_btn.drawButton(false);
    off_btn.drawButton(false);
    zero_btn.drawButton(false);
    
   tft.fillRect(61, 9, 3, 10, WHITE);//batarya ucu

    
   tft.fillRoundRect(1, 1, 60, 25,1, WHITE);//batarya şase
 
   tft.fillRoundRect(2, 2, 58, 23,3, BLACK);//batarya içi

   
      //SICAKLIK GÖSTERGE
     //  tft.fillCircle(450,100,20,BLACK);
    //  tft.fillRoundRect(441, 17, 19, 70,3, BLACK);
   // tft.fillCircle(450,100,18,WHITE);
  // tft.fillRoundRect(443, 19, 15, 69,3, WHITE);

  

 tft.fillRoundRect(98, 5, 340, 30,12, WHITE);  // BAŞLIK
 
 tft.fillRoundRect(441, 31, 19, 4,3, ORANGE); //LOGO
 tft.fillRoundRect(464, 31, 12, 4,3, ORANGE); //LOGO
 
 tft.fillRoundRect(446, 23, 7, 4,3, ORANGE); //LOGO
 tft.fillRoundRect(458, 23, 14, 4,3, ORANGE); //LOGO

 tft.fillRoundRect(450, 15, 9, 4,3, ORANGE); //LOGO
 tft.fillRoundRect(463, 15, 5, 4,3, ORANGE); //LOGO

 tft.fillRoundRect(454, 7, 9, 4,3, ORANGE); //LOGO

 
 //tft.setFont(&FreeSerif9pt7b);
 tft.setFont(&FreeSerifItalic12pt7b);
   
    tft.setTextColor( ORANGE,WHITE);
    tft.setTextSize(1);
    tft.setCursor(113, 25);
    tft.print("APPCENT AEROSPACE TECH.");
    
 tft.setFont(&FreeSerifBold12pt7b);
    tft.fillRoundRect(246, 45, 230, 30,5, WHITE);    
    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor(253, 68);
    tft.print("GROUND STATION");


    tft.fillRoundRect(2, 45, 230, 30,5, WHITE);    
    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor(70, 68);
    tft.print("ROCKET");

    //GROUND STATION DATA TABLO

    tft.fillRoundRect((g_stnalt_x), (g_stnalt_y), (g_stnalt_en), (g_stnalt_boy), 3, CYAN);
    
    tft.fillRoundRect((g_stnalt_x + 4),(g_stnalt_y + 4),(g_stnalt_en - 8), (g_barboy), 2, BLACK);                             //bar dış çerçeve
    tft.fillRoundRect((g_stnalt_x + 8),(g_stnalt_y + 8),((g_stnalt_en/2) - 12), (g_barboy - 8), 3, WHITE);                    //bar içi 1.
    tft.fillRoundRect((g_stnalt_x + 4 +((g_stnalt_en/2))),(g_stnalt_y + 8),((g_stnalt_en/2) - 12), (g_barboy - 8), 3, WHITE);  //bar içi 2.
   
    tft.fillRoundRect((g_stnalt_x + 4),(g_stnalt_y + 8 + g_barboy),(g_stnalt_en - 8), (g_barboy), 2, BLACK);                              //bar dış çerçeve
    tft.fillRoundRect((g_stnalt_x + 8),(g_stnalt_y + 12 + g_barboy),((g_stnalt_en/2) - 12), (g_barboy - 8), 3, WHITE);                    //bar içi 1.
    tft.fillRoundRect((g_stnalt_x + 4 + ((g_stnalt_en/2))),(g_stnalt_y + 12 + g_barboy), ((g_stnalt_en/2) - 12), (g_barboy - 8), 3, WHITE); //bar içi 2.

    tft.fillRoundRect((g_stnalt_x + 4),(g_stnalt_y + 12 + (g_barboy*2)),(g_stnalt_en - 8), (g_barboy), 2, BLACK);                             //bar dış çerçeve
    tft.fillRoundRect((g_stnalt_x + 8),(g_stnalt_y + 16 + (g_barboy*2)),((g_stnalt_en/2) - 12), (g_barboy - 8), 3, WHITE);                    //bar içi 1.
    tft.fillRoundRect((g_stnalt_x + 4 + ((g_stnalt_en/2))),(g_stnalt_y + 16 + (g_barboy*2)), ((g_stnalt_en/2) - 12), (g_barboy - 8), 3, WHITE); //bar içi 2.
    
    tft.fillRoundRect((g_stnalt_x + 4),(g_stnalt_y + 16 + (g_barboy*3)),(g_stnalt_en - 8), (g_barboy), 2, BLACK);                             //bar dış çerçeve
    tft.fillRoundRect((g_stnalt_x + 8),(g_stnalt_y + 20 + (g_barboy*3)),((g_stnalt_en/2) - 12), (g_barboy - 8), 3, WHITE);                    //bar içi 1.
    tft.fillRoundRect((g_stnalt_x + 4 + ((g_stnalt_en/2))),(g_stnalt_y + 20 + (g_barboy*3)), ((g_stnalt_en/2) - 12), (g_barboy - 8), 3, WHITE); //bar içi 2.




// İLK SATIRLAR
    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor((g_stnalt_x + 17), 63+45);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("TEMP.");

    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor((g_stnalt_x + 15), 66+ g_barboy+45);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("ALTITUDE");

    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor((g_stnalt_x + 15), 72 +(g_barboy*2)+45);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("GPS");

    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor((g_stnalt_x + 15), 75 +(g_barboy*3)+45);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("DISTANCE");


// İKİNCİ SATIRLAR
    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor(g_stnalt_x + 20 + (g_stnalt_en/2), 63+45);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("32 ");
    tft.drawCircle(g_stnalt_x + 45 + (g_stnalt_en/2), 51+45,2, BLACK);
    tft.print(" C");
    

    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor(g_stnalt_x + 20 + (g_stnalt_en/2), 66+ g_barboy+45);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("5 M");

    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor(g_stnalt_x + 10 + (g_stnalt_en/2), 72 +(g_barboy*2)+45);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("40°14'04.9'N'");

    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor(g_stnalt_x + 10 + (g_stnalt_en/2), 75 +(g_barboy*3)+45);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("20 METER");



// ROKETTEN GELEN VERİ TABLO

    tft.fillRoundRect((r_stnalt_x), (r_stnalt_y), (r_stnalt_en), (r_stnalt_boy), 3, CYAN);

    tft.fillRoundRect((r_stnalt_x + 4),(r_stnalt_y + 4),(r_stnalt_en - 8), (r_barboy), 2, BLACK);                             //bar dış çerçeve
    tft.fillRoundRect((r_stnalt_x + 8),(r_stnalt_y + 8),((r_stnalt_en/2) - 12), (r_barboy - 8), 3, WHITE);                    //bar içi 1.
    tft.fillRoundRect((r_stnalt_x + 4 +((r_stnalt_en/2))),(r_stnalt_y + 8),((r_stnalt_en/2) - 12), (r_barboy - 8), 3, WHITE);  //bar içi 2.

    tft.fillRoundRect((r_stnalt_x + 4),(r_stnalt_y + 8 + r_barboy),(r_stnalt_en - 8), (r_barboy), 2, BLACK);                              //bar dış çerçeve
    tft.fillRoundRect((r_stnalt_x + 8),(r_stnalt_y + 12 + r_barboy),((r_stnalt_en/2) - 12), (r_barboy - 8), 3, WHITE);                    //bar içi 1.
    tft.fillRoundRect((r_stnalt_x + 4 + ((r_stnalt_en/2))),(r_stnalt_y + 12 + r_barboy), ((r_stnalt_en/2) - 12), (r_barboy - 8), 3, WHITE); //bar içi 2.

    tft.fillRoundRect((r_stnalt_x + 4),(r_stnalt_y + 12 + (r_barboy*2)),(r_stnalt_en - 8), (r_barboy*2 +3), 2, BLACK);                             //bar dış çerçeve
    tft.fillRoundRect((r_stnalt_x + 8),(r_stnalt_y + 16 + (r_barboy*2)),((r_stnalt_en/2) - 12), (r_barboy*2 - 5), 3, WHITE);                    //bar içi 1.
    tft.fillRoundRect((r_stnalt_x + 4 + ((r_stnalt_en/2))),(r_stnalt_y + 16 + (r_barboy*2)), ((r_stnalt_en/2) - 12), (r_barboy*2 - 5), 3, WHITE); //bar içi 2.
    
    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor((r_stnalt_x + 17), 63+45);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("TEMP.");

    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor((r_stnalt_x + 15), 66+ r_barboy+45);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("ALTITUDE");

    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor((r_stnalt_x + 15), 72 +(r_barboy*2)+70);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("GPS");



// İKİNCİ SATIRLAR
    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor(r_stnalt_x + 20 + (r_stnalt_en/2), 63+45);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("15 ");
    tft.drawCircle(r_stnalt_x + 45 + (r_stnalt_en/2), 51+45,2, BLACK);
    tft.print(" C");
    

    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor(r_stnalt_x + 20 + (r_stnalt_en/2), 66+ r_barboy+45);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("3200 M");

    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor(r_stnalt_x + 9 + (r_stnalt_en/2), 72 +(r_barboy*2)+45);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("40 14'05.7'N'");

    tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor(r_stnalt_x + 9 + (r_stnalt_en/2), 75 +(r_barboy*3)+35);
    tft.setFont(&FreeSerif9pt7b);
    tft.print("35 10'00.5'E'");

    
}

/* two buttons are quite simple
 */
void loop(void)
{

 
  
    bool down = Touch_getXY();
    on_btn.press(down && on_btn.contains(pixel_x, pixel_y));
    off_btn.press(down && off_btn.contains(pixel_x, pixel_y));
    zero_btn.press(down && zero_btn.contains(pixel_x, pixel_y));
     
    if (on_btn.justReleased())
        on_btn.drawButton();
        
    if (zero_btn.justReleased())
        zero_btn.drawButton();
        
    if (off_btn.justReleased())
        off_btn.drawButton();

   if (zero_btn.justPressed()) {
       zero_btn.drawButton(true);
       plus1=0;
       tft.fillRoundRect(2, 2, 58, 23,3, BLACK);
    }
    
  if (on_btn.justPressed()) {
      on_btn.drawButton(true);
        tft.fillRoundRect(2, 2, plus1, 23,3, sarz);
      plus1 =plus1+1;
        if(plus1>=58) plus1=58;
      
    }
    
    if (off_btn.justPressed()) {
        off_btn.drawButton(true);
         tft.fillRoundRect(2, 2, plus1, 23,3, BLACK);     // bu kodun satırını sakın bozma. 
        plus1 =plus1-1;
        if(plus1<=0) plus1=0;
      
        
    }

    
 //  tft.fillCircle(450,100,16,RED);
  // tft.fillRoundRect(444, 20, 13, degree,3, RED);



 tft.fillRoundRect(2, 2, plus1, 23,3, sarz);
  Serial.println(plus1);

     a = (plus1*100);
     percent=(a/58);
     tft.setFont();
       Serial.print(percent);
   tft.setTextColor( WHITE,BLACK);
    tft.setTextSize(1);
    tft.setCursor(68, 12);
    
    tft.print("%");
   tft.print(percent,1);
   tft.print(" ");


if(percent<=20)
{ 
 sarz = RED;

}
else sarz = GREEN;


   
      
}
